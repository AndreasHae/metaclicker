package com.metaclicker;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Metaclicker extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        Application.setUserAgentStylesheet(Application.STYLESHEET_MODENA);

        SceneSwitcher.init(primaryStage);
        SceneSwitcher.get().goToMain();

        Parent root = FXMLLoader.load(getClass().getResource("/fxml/main.fxml"));
        Scene mainScene = new Scene(root);
        mainScene.getStylesheets().add("/css/base.css");

        primaryStage.setTitle("Metaclicker");
        primaryStage.setScene(mainScene);
        primaryStage.setMaximized(true);
        primaryStage.show();
    }
}
