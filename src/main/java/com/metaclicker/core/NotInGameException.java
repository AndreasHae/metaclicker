package com.metaclicker.core;

/**
 * Thrown when something is trying to be accessed from a playthrough which is not available in the game the playthrough
 * is taking place in.
 */
public class NotInGameException extends RuntimeException {

    /**
     * Class constructor.
     *
     * @param thing the thing that is trying to be accessed
     */
    public NotInGameException(String thing) {
        super(thing + " is trying to be accessed, although it is not part of the current game");
    }
}
