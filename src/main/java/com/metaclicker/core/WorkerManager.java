package com.metaclicker.core;

import javafx.application.Platform;
import javafx.beans.property.LongProperty;
import javafx.beans.property.MapProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleMapProperty;
import javafx.beans.value.ObservableLongValue;
import javafx.collections.FXCollections;
import javafx.collections.MapChangeListener;

import java.io.Serializable;
import java.util.Collections;
import java.util.Map;
import java.util.Objects;

/**
 * This class caters for all workers in a playthrough.
 */
public class WorkerManager implements Serializable {

    // TODO 2017-06-07: Fix serialization

    private static final double COST_MODIFIER = 1.15;

    private final Playthrough playthrough;
    private MapProperty<Worker, LongProperty> workers;
    private LongProperty pointsPerSecond;

    /**
     * Class constructor.
     *
     * @param playthrough the playthrough that the WorkerManager should be bound to
     */
    public WorkerManager(Playthrough playthrough) {
        this.playthrough = playthrough;
        this.workers = new SimpleMapProperty<>(FXCollections.observableHashMap());
        this.workers.addListener((MapChangeListener<Worker, LongProperty>) change -> this.pointsPerSecond.set(calculatePointsPerSecond()));
        this.pointsPerSecond = new SimpleLongProperty();

        Thread pointAddingThread = new Thread(this::incrementPoints, "increment-points");
        pointAddingThread.setDaemon(true);
        pointAddingThread.start();
    }

    private void incrementPoints() {
        while (true) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Platform.runLater(() -> playthrough.setPoints(playthrough.getPoints() + getPointsPerSecond()));
        }
    }

    public long getPointsPerSecond() {
        return pointsPerSecond.get();
    }

    public LongProperty pointsPerSecondProperty() {
        return pointsPerSecond;
    }

    private long calculatePointsPerSecond() {
        return workers
                .entrySet()
                .stream()
                .mapToLong(entry -> entry.getValue().get() * entry.getKey().getPps())
                .sum();
    }

    public Map<Worker, ObservableLongValue> getWorkersInAmounts() {
        return Collections.unmodifiableMap(workers);
    }

    /**
     * Makes the given type of worker available to buy.
     *
     * @param worker the worker to be made available
     * @throws NullPointerException if the given worker is <code>null</code>
     * @throws NotInGameException   if the given worker is not part of the game
     */
    public void makeAvailable(Worker worker) {
        if (worker == null) {
            throw new NullPointerException("Worker must not be null");
        }

        if (!playthrough.getGame().getWorkers().contains(worker)) {
            throw new NotInGameException(worker.toString());
        }

        LongProperty amount = new SimpleLongProperty(0);
        amount.addListener((observable, oldValue, newValue) -> pointsPerSecond.set(calculatePointsPerSecond()));
        workers.put(worker, amount);
    }

    /**
     * Increases amount of bought workers of the given type by one and subtracts the price of them from the available
     * cookies.
     *
     * @param worker The worker to be bought
     * @throws NullPointerException      if the given worker is <code>null</code>
     * @throws NotInGameException        if the worker is not part of the game
     * @throws IllegalStateException     if the worker is not available for buying
     * @throws NotEnoughPointsException if there are not enough cookies to buy the worker
     */
    public void buy(Worker worker) throws NotEnoughPointsException {
        if (worker == null) {
            throw new NullPointerException("Worker must not be null");
        }

        if (!playthrough.getGame().getWorkers().contains(worker)) {
            throw new NotInGameException(worker.toString());
        }

        if (getNumberOfWorkers(worker) == -1) {
            throw new IllegalStateException(worker.toString() + " is not available for buying");
        }

        if (playthrough.getPoints() - worker.getPrice() < 0) {
            throw new NotEnoughPointsException();
        }

        LongProperty amount = workers.get(worker);
        playthrough.setPoints(playthrough.getPoints() - worker.getPrice());
        workers.remove(worker);
        amount.set(amount.get() + 1);
        worker.setPrice(Math.round(worker.getPrice() * Math.pow(COST_MODIFIER, amount.get())));
        workers.put(worker, amount);
    }

    /**
     * Returns the number of given workers bought.
     *
     * @param worker the type of worker to be queried
     * @return the number of workers bought or <code>-1</code> if the given worker is unavailable
     * @throws NullPointerException if the given worker is null
     * @throws NotInGameException   if the given worker is not part of the game
     */
    public long getNumberOfWorkers(Worker worker) {
        if (worker == null) {
            throw new NullPointerException("Worker must not be null");
        }

        if (!playthrough.getGame().getWorkers().contains(worker)) {
            throw new NotInGameException(worker.toString());
        }

        LongProperty numberOfWorkers = workers.get(worker);
        if (numberOfWorkers != null) {
            return numberOfWorkers.get();
        } else {
            return -1;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WorkerManager that = (WorkerManager) o;
        return Objects.equals(playthrough.getGame(), that.playthrough.getGame()) &&
                Objects.equals(playthrough.getPoints(), that.playthrough.getPoints()) &&
                Objects.equals(workers, that.workers);
    }

    @Override
    public int hashCode() {
        return Objects.hash(playthrough, workers);
    }

    @Override
    public String toString() {
        return "WorkerManager{" +
                "playthrough=" + playthrough +
                ", workers=" + workers +
                '}';
    }
}
