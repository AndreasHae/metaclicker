package com.metaclicker.core;

/**
 * Thrown when trying to buy something without having the necessary amount of points
 */
public class NotEnoughPointsException extends Exception {
}
