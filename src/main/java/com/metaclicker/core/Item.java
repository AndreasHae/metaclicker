package com.metaclicker.core;

import javafx.beans.property.LongProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Objects;

/**
 * A thing that increases the amount of cookies received from clicking.
 */
public class Item implements Serializable {

    private StringProperty name;
    private LongProperty clickIncrease;
    private LongProperty price;

    /**
     * Class constructor.
     *
     * @param name          the name of the item
     * @param clickIncrease the amount of cookies by which the worth of a click is increased
     * @param price         the amount of cookies that have to be paid when buying the item
     * @throws NullPointerException if the given name is <code>null</code>
     */
    public Item(String name, long clickIncrease, long price) {
        this.name = new SimpleStringProperty(name);
        this.clickIncrease = new SimpleLongProperty(clickIncrease);
        this.price = new SimpleLongProperty(price);
    }

    /**
     * @return the value of {@link #clickIncreaseProperty()}
     */
    public long getClickIncrease() {
        return clickIncrease.get();
    }

    /**
     * The amount of cookies by which the click value is increased.
     */
    public LongProperty clickIncreaseProperty() {
        return clickIncrease;
    }

    /**
     * @param clickIncrease the new value for {@link #clickIncreaseProperty()}
     */
    public void setClickIncrease(long clickIncrease) {
        this.clickIncrease.set(clickIncrease);
    }

    /**
     * @return the value of {@link #priceProperty()}
     */
    public long getPrice() {
        return price.get();
    }

    /**
     * The amount of cookies that have to be paid when buying the item.
     */
    public LongProperty priceProperty() {
        return price;
    }

    /**
     * @param price the new value for {@link #priceProperty()}
     */
    public void setPrice(long price) {
        this.price.set(price);
    }

    /**
     * @return the value of {@link #nameProperty()}
     */
    public String getName() {
        return name.get();
    }

    /**
     * The name of the item.
     */
    public StringProperty nameProperty() {
        return name;
    }

    /**
     * @param name the new value for {@link #nameProperty()}
     */
    public void setName(String name) {
        this.name.set(name);
    }

    private void writeObject(ObjectOutputStream s) throws IOException {
        s.writeObject(name.get());
        s.writeLong(clickIncrease.get());
        s.writeLong(price.get());
    }

    private void readObject(ObjectInputStream s) throws IOException, ClassNotFoundException {
        name = new SimpleStringProperty((String) s.readObject());
        clickIncrease = new SimpleLongProperty(s.readLong());
        price = new SimpleLongProperty(s.readLong());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Item item = (Item) o;

        return name.get().equals(item.name.get())
                && clickIncrease.get() == item.clickIncrease.get()
                && price.get() == item.price.get();
    }

    @Override
    public int hashCode() {
        return Objects.hash(name.get(), clickIncrease.get(), price.get());
    }

    @Override
    public String toString() {
        return "Item{" +
                "name=" + name.get() +
                ", clickIncrease=" + clickIncrease.get()+
                ", price=" + price.get() +
                '}';
    }
}
