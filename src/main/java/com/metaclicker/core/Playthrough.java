package com.metaclicker.core;

import javafx.beans.property.LongProperty;
import javafx.beans.property.SimpleLongProperty;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Objects;

public class Playthrough implements Serializable {

    private final Game game;
    private final WorkerManager workerManager;
    private final ItemManager itemManager;
    private transient LongProperty points;

    public Playthrough(Game game) {
        if (game == null) {
            throw new NullPointerException("Game must not be null");
        }

        this.game = game;
        this.workerManager = new WorkerManager(this);
        this.itemManager = new ItemManager(this);
        this.points = new SimpleLongProperty();
    }

    public void incrementPointsByClick() {
        setPoints(getPoints() + itemManager.getClickValue());
    }

    public long getPoints() {
        return points.get();
    }

    public void setPoints(long points) {
        this.points.set(points);
    }

    /**
     * The current points of the game.
     *
     * @return the points property
     */
    public LongProperty pointsProperty() {
        return points;
    }

    /**
     * @return the {@link Game} in which this playthrough is taking place
     */
    public Game getGame() {
        return game;
    }

    /**
     * @return the {@link WorkerManager} of the game
     */
    public WorkerManager getWorkerManager() {
        return workerManager;
    }

    /**
     * @return the {@link ItemManager} of the game
     */
    public ItemManager getItemManager() {
        return itemManager;
    }

    private void writeObject(ObjectOutputStream s) throws IOException {
        s.defaultWriteObject();
        s.writeLong(points.get());
    }

    private void readObject(ObjectInputStream s) throws IOException, ClassNotFoundException {
        s.defaultReadObject();
        points = new SimpleLongProperty(s.readLong());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Playthrough that = (Playthrough) o;
        return Objects.equals(game, that.game) &&
                Objects.equals(workerManager, that.workerManager) &&
                Objects.equals(itemManager, that.itemManager) &&
                Objects.equals(points.get(), that.points.get());
    }

    @Override
    public int hashCode() {
        return Objects.hash(game, workerManager, itemManager, points);
    }

    @Override
    public String toString() {
        return "Playthrough{" +
                "game=" + game +
                ", workerManager=" + workerManager +
                ", itemManager=" + itemManager +
                ", points=" + points +
                '}';
    }
}
