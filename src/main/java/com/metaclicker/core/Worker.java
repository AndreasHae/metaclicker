package com.metaclicker.core;

import javafx.beans.property.LongProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Objects;

/**
 * A thing that generates cookies per second.
 */
public class Worker implements Serializable {

    private transient StringProperty name;
    private transient LongProperty pps;
    private transient LongProperty price;

    /**
     * Class constructor.
     *
     * @param name  the name of the worker
     * @param pps   the amount of cookies the worker generates per second
     * @param price the amount of cookies that have to be paid when buying the worker
     * @throws NullPointerException if the given name is <code>null</code>
     */
    public Worker(String name, long pps, long price) {
        this.name = new SimpleStringProperty(name);
        this.pps = new SimpleLongProperty(pps);
        this.price = new SimpleLongProperty(price);
    }

    /**
     * @return the value of {@link #nameProperty()}
     */
    public String getName() {
        return name.get();
    }

    /**
     * Sets the value of {@link #nameProperty()}.
     *
     * @param name the new value
     */
    public void setName(String name) {
        this.name.set(name);
    }

    /**
     * The name of the worker.
     */
    public StringProperty nameProperty() {
        return name;
    }

    /**
     * @return the value of {@link #ppsProperty()}
     */
    public long getPps() {
        return pps.get();
    }

    /**
     * Sets the value of {@link #ppsProperty()}.
     *
     * @param pps the new value
     */
    public void setPps(long pps) {
        this.pps.set(pps);
    }

    /**
     * The amount of points this worker generates per second.
     */
    public LongProperty ppsProperty() {
        return pps;
    }

    /**
     * @return the value of {@link #priceProperty()}
     */
    public long getPrice() {
        return price.get();
    }

    /**
     * Sets the value of {@link #priceProperty()}.
     *
     * @param price the new value
     */
    public void setPrice(long price) {
        this.price.set(price);
    }

    /**
     * The amount of points this worker costs when bought.
     */
    public LongProperty priceProperty() {
        return price;
    }

    private void writeObject(ObjectOutputStream s) throws IOException {
        s.writeObject(name.get());
        s.writeLong(pps.get());
        s.writeLong(price.get());
    }

    private void readObject(ObjectInputStream s) throws IOException, ClassNotFoundException {
        name = new SimpleStringProperty((String) s.readObject());
        pps = new SimpleLongProperty(s.readLong());
        price = new SimpleLongProperty(s.readLong());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Worker worker = (Worker) o;

        return pps.get() == worker.pps.get()
                && price.get() == worker.price.get()
                && name.get().equals(worker.name.get());
    }

    @Override
    public int hashCode() {
        return Objects.hash(name.get(), pps.get(), price.get());
    }

    @Override
    public String toString() {
        return "Worker{" +
                "name='" + name + '\'' +
                ", pps=" + pps +
                ", price=" + price +
                '}';
    }
}
