package com.metaclicker.core;

import javafx.beans.property.LongProperty;
import javafx.beans.property.MapProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleMapProperty;
import javafx.collections.FXCollections;
import javafx.collections.MapChangeListener;
import javafx.collections.ObservableList;

import java.io.Serializable;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * This class caters for all items in a playthrough.
 */
public class ItemManager implements Serializable {

    // TODO 2017-06-07: Fix serialization

    private Playthrough playthrough;
    private MapProperty<Item, Boolean> items; // key = the item, value = if the item has been bought
    private LongProperty clickValue;

    /**
     * Class constructor.
     *
     * @param playthrough the playthrough the ItemManager should be bound to
     */
    public ItemManager(Playthrough playthrough) {
        if (playthrough == null) {
            throw new NullPointerException("Playthrough must not be null");
        }
        this.playthrough = playthrough;
        this.items = new SimpleMapProperty<>(FXCollections.observableHashMap());
        this.items.addListener((MapChangeListener<Item, Boolean>) change -> this.clickValue.set(calculateClickValue()));
        this.clickValue = new SimpleLongProperty();
    }

    public long getClickValue() {
        return clickValue.get();
    }

    public LongProperty clickValueProperty() {
        return clickValue;
    }

    private long calculateClickValue() {
        return items
                .entrySet()
                .stream()
                .mapToLong(entry -> entry.getValue() ? entry.getKey().getClickIncrease() : 0)
                .sum() + 1;
    }

    /**
     * @return an unmodifiable list of all items that have not been bought yet
     */
    public ObservableList<Item> getUnboughtItems() {
        return FXCollections.unmodifiableObservableList(FXCollections.observableList(
                items
                        .entrySet()
                        .stream()
                        .filter(itemBought -> !itemBought.getValue())
                        .map(Map.Entry::getKey)
                        .sorted((o1, o2) -> (int) (o1.getPrice() - o2.getPrice()))
                        .collect(Collectors.toList())
        ));
    }

    /**
     * Makes the given item available to buy.
     *
     * @param item the item to be made available
     * @throws NullPointerException if the given item is <code>null</code>
     * @throws NotInGameException   if the item is not part of the game
     */
    public void makeAvailable(Item item) {
        if (item == null) {
            throw new NullPointerException("Item must not be null");
        }

        if (!playthrough.getGame().getItems().contains(item)) {
            throw new NotInGameException(item.toString());
        }

        items.put(item, false);
    }

    /**
     * Marks the item as bought and subtracts the price of it from the available cookies.
     *
     * @param item the item to be bought
     * @throws NullPointerException      if the item is <code>null</code>
     * @throws NotInGameException        if the item is not part of the game
     * @throws IllegalStateException     if the item is not available to buy
     * @throws NotEnoughPointsException if there are not enough cookies to buy the item
     */
    public void buyItem(Item item) throws NotEnoughPointsException {
        if (item == null) {
            throw new NullPointerException("Item must not be null");
        }

        if (!playthrough.getGame().getItems().contains(item)) {
            throw new NotInGameException(item.toString());
        }

        if (!isItemAvailable(item)) {
            throw new IllegalStateException("Item is not available to buy");
        }

        if (playthrough.getPoints() - item.getPrice() < 0) {
            throw new NotEnoughPointsException();
        }

        playthrough.setPoints(playthrough.getPoints() - item.getPrice());
        items.put(item, true);
    }

    /**
     * Checks if the given item is available to buy.
     *
     * @param item the item that should be checked
     * @return <code>true</code> if the item is available to buy, otherwise <code>false</code>
     * @throws NullPointerException if the given item is <code>null</code>
     * @throws NotInGameException   if the given item is not part of the game
     */
    public boolean isItemAvailable(Item item) {
        if (item == null) {
            throw new NullPointerException("Item must not be null");
        }

        if (!playthrough.getGame().getItems().contains(item)) {
            throw new NotInGameException(item.toString());
        }

        return items.containsKey(item);
    }

    /**
     * Checks if the given item is already bought.
     *
     * @param item the item to be checked
     * @return <code>true</code> if the item is bought, otherwise <code>false</code>
     * @throws NullPointerException if the given item is <code>null</code>
     * @throws NotInGameException   if the given item is not part of the game
     */
    public boolean isItemBought(Item item) {
        if (item == null) {
            throw new NullPointerException("Item must not be null");
        }

        if (!playthrough.getGame().getItems().contains(item)) {
            throw new NotInGameException(item.toString());
        }

        Boolean isBought = items.get(item);
        if (isBought == null) {
            return false;
        }
        return isBought;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ItemManager that = (ItemManager) o;
        return Objects.equals(playthrough.getGame(), that.playthrough.getGame()) &&
                Objects.equals(playthrough.getPoints(), that.playthrough.getPoints()) &&
                Objects.equals(items, that.items);
    }

    @Override
    public int hashCode() {
        return Objects.hash(playthrough, items);
    }

    @Override
    public String toString() {
        return "ItemManager{" +
                "playthrough=" + playthrough +
                ", items=" + items +
                '}';
    }
}
