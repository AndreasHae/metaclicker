package com.metaclicker.core;

import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Objects;

/**
 * The clicker game.
 */
public class Game implements Serializable {

    private static final long serialVersionUID = 1;

    private transient StringProperty name;
    private transient ListProperty<Worker> workers;
    private transient ListProperty<Item> items;

    /**
     * Class constructor.
     */
    public Game(String name) {
        this.name = new SimpleStringProperty(name);

        ObservableList<Worker> workerList = FXCollections.observableArrayList();
        this.workers = new SimpleListProperty<>(workerList);

        ObservableList<Item> itemList = FXCollections.observableArrayList();
        this.items = new SimpleListProperty<>(itemList);
    }

    /**
     * @return the value of {@link #workersProperty()}
     */
    public ObservableList<Worker> getWorkers() {
        return workers.get();
    }

    /**
     * All {@link Worker}s of the game.
     *
     * @return the workers property
     */
    public ListProperty<Worker> workersProperty() {
        return workers;
    }

    /**
     * @return the value of {@link #itemsProperty()}
     */
    public ObservableList<Item> getItems() {
        return items.get();
    }

    /**
     * All {@link Item}s of the game.
     *
     * @return the items property
     */
    public ListProperty<Item> itemsProperty() {
        return items;
    }

    /**
     * @return the value of {@link #nameProperty()}
     */
    public String getName() {
        return name.get();
    }

    /**
     * @param name the new value for {@link #nameProperty()}
     */
    public void setName(String name) {
        this.name.set(name);
    }

    /**
     * The name of the game.
     *
     * @return the name property
     */
    public StringProperty nameProperty() {
        return name;
    }

    private void writeObject(ObjectOutputStream s) throws IOException {
        s.defaultWriteObject();
        s.writeObject(name.get());
        s.writeObject(new ArrayList<>(workers.get()));
        s.writeObject(new ArrayList<>(items.get()));
    }

    @SuppressWarnings("unchecked")
    private void readObject(ObjectInputStream s) throws IOException, ClassNotFoundException {
        s.defaultReadObject();
        name = new SimpleStringProperty((String) s.readObject());
        workers = new SimpleListProperty<>(FXCollections.observableArrayList((ArrayList<Worker>) s.readObject()));
        items = new SimpleListProperty<>(FXCollections.observableArrayList((ArrayList<Item>) s.readObject()));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Game game = (Game) o;
        return Objects.equals(name.get(), game.name.get());
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public String toString() {
        return "Game{" +
                "name=" + name +
                ", workers=" + workers +
                ", items=" + items +
                '}';
    }
}
