package com.metaclicker;

import com.metaclicker.controls.player.PlayerView;
import com.metaclicker.core.Game;
import com.metaclicker.core.Playthrough;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;

public class SceneSwitcher {

    private static SceneSwitcher instance;

    private final Stage stage;

    private SceneSwitcher(Stage stage) {
        this.stage = stage;
    }

    public static void init(Stage stage) {
        if (instance != null) {
            throw new IllegalStateException("SceneSwitcher is already initialized.");
        }
        instance = new SceneSwitcher(stage);
    }

    public static SceneSwitcher get() {
        if (instance == null) {
            throw new IllegalStateException("SceneSwitcher is not yet initialized. Call SceneSwitcher.init(Stage) first!");
        }
        return instance;
    }

    public void goToMain() {
        Scene mainScene = getScene(getClass().getResource("/fxml/main.fxml"));
        mainScene.getStylesheets().add("/css/main.css");
        goToScene(mainScene);
    }

    public void goToPlayer(Game game) {
        Scene playerScene = new Scene(new PlayerView(game));
        goToScene(playerScene);
    }

    public void goToPlayer(Playthrough playthrough) {
        Scene playerScene = new Scene(new PlayerView(playthrough));
        goToScene(playerScene);
    }

    private void goToScene(Scene scene) {
        boolean prevMaximized = stage.isMaximized();
        stage.setMaximized(false);
        stage.setScene(scene);
        stage.setMaximized(prevMaximized); // workaround for window not having full size after switching scenes
    }

    private Scene getScene(URL rootFxml) {
        Parent root;
        try {
            root = FXMLLoader.load(rootFxml);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return new Scene(root);
    }

}
