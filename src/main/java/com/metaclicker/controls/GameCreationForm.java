package com.metaclicker.controls;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;

import java.io.IOException;

public class GameCreationForm extends GridPane {

    @FXML
    private Button btnCreate;

    public GameCreationForm() {
        Utils.initFxmlElement(this, "/fxml/game-creation-form.fxml");
    }

}
