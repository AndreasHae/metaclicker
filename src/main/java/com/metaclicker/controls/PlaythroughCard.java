package com.metaclicker.controls;

import javafx.beans.property.StringProperty;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;

import java.io.IOException;

public class PlaythroughCard extends VBox {

    @FXML
    private Label lblName;
    @FXML
    private Label lblPlaytime;
    @FXML
    private Label lblScore;

    @FXML
    private Button btnResume;

    public PlaythroughCard() {
        this("unnamed", "00:00", "0");
    }

    public PlaythroughCard(String name, String playtime, String score) {
        Utils.initFxmlElement(this, "/fxml/playthrough-card.fxml");

        assert lblName != null;
        assert lblPlaytime != null;
        assert lblScore != null;
        assert btnResume != null;

        setName(name);
        setPlaytime(playtime);
        setScore(score);
    }

    public String getName() {
        return lblName.getText();
    }

    public void setName(String name) {
        if (name == null) {
            throw new NullPointerException("Name must not be null");
        }
        lblName.setText(name);
    }

    public StringProperty nameProperty() {
        return lblName.textProperty();
    }

    public String getPlaytime() {
        return lblPlaytime.getText();
    }

    public void setPlaytime(String playtime) {
        if (playtime == null) {
            throw new NullPointerException("Playtime must not be null");
        }
        lblPlaytime.setText(playtime);
    }

    public StringProperty playtimeProperty() {
        return lblPlaytime.textProperty();
    }

    public String getScore() {
        return lblScore.getText();
    }

    public void setScore(String score) {
        if (score == null) {
            throw new NullPointerException("Score must not be null");
        }
        lblScore.setText(score);
    }

    public StringProperty scoreProperty() {
        return lblScore.textProperty();
    }

}
