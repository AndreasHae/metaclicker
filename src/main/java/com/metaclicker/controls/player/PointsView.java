package com.metaclicker.controls.player;

import com.metaclicker.controls.Utils;
import javafx.beans.binding.Bindings;
import javafx.beans.property.LongProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Circle;

public class PointsView extends VBox {

    @FXML
    private Label lblPoints;
    @FXML
    private Label lblPointsPerSecond;
    @FXML
    private Circle clickable;

    private LongProperty points;
    private LongProperty pointsPerSecond;
    private ObjectProperty<EventHandler<MouseEvent>> onClick;

    public PointsView() {
        this(0, 0);
    }

    public PointsView(long points, long pointsPerSecond) {
        Utils.initFxmlElement(this, "/fxml/player/points-view.fxml");

        this.points = new SimpleLongProperty(points);
        this.pointsPerSecond = new SimpleLongProperty(pointsPerSecond);

        lblPoints.textProperty().bind(Bindings.convert(this.points).concat(" points"));
        lblPointsPerSecond.textProperty().bind(Bindings.convert(this.pointsPerSecond).concat(" pps"));
        clickable.onMouseClickedProperty();
    }

    public EventHandler<? super MouseEvent> getOnClick() {
        return clickable.getOnMouseClicked();
    }

    public void setOnClick(EventHandler<? super MouseEvent> onClick) {
        clickable.setOnMouseClicked(onClick);
    }

    public ObjectProperty<EventHandler<? super MouseEvent>> onClickProperty() {
        return clickable.onMouseClickedProperty();
    }

    public long getPoints() {
        return points.get();
    }

    public void setPoints(long points) {
        this.points.set(points);
    }

    public LongProperty pointsProperty() {
        return points;
    }

    public long getPointsPerSecond() {
        return pointsPerSecond.get();
    }

    public void setPointsPerSecond(long pointsPerSecond) {
        this.pointsPerSecond.set(pointsPerSecond);
    }

    public LongProperty pointsPerSecondProperty() {
        return pointsPerSecond;
    }
}
