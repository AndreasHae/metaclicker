package com.metaclicker.controls.player;

import com.metaclicker.controls.Utils;
import com.metaclicker.core.Item;
import com.metaclicker.core.ItemManager;
import com.metaclicker.core.NotEnoughPointsException;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.HBox;

public class ItemShop extends ScrollPane {

    @FXML
    private HBox content;

    private ItemManager itemManager;

    public ItemShop() {
        this(null);
    }

    public ItemShop(ItemManager itemManager) {
        Utils.initFxmlElement(this, "/fxml/player/item-shop.fxml");
        setItemManager(itemManager);
    }

    public ItemManager getItemManager() {
        return itemManager;
    }

    public void setItemManager(ItemManager itemManager) {
        this.itemManager = itemManager;
        if (this.itemManager != null) {
            this.itemManager.getUnboughtItems().forEach(this::addItemButton);
        }
    }

    private void addItemButton(Item item) {
        Button itemButton = new Button();
        itemButton.setPrefSize(100, 100);
        itemButton.textProperty().bind(item.nameProperty());

        Tooltip tt = new Tooltip();
        tt.textProperty().bind(
                item.nameProperty()
                        .concat("\nIncrease: ")
                        .concat(item.clickIncreaseProperty())
                        .concat("\nPrice: ")
                        .concat(item.priceProperty())
        );
        itemButton.setTooltip(tt);

        itemButton.setOnAction(event -> {
            try {
                itemManager.buyItem(item);
                content.getChildren().remove(itemButton);
            } catch (NotEnoughPointsException e) {
                itemButton.setStyle("-fx-border-color: red");

                Thread thread = new Thread(() -> {
                    try {
                        Thread.sleep(1000);
                        itemButton.setStyle(null);
                    } catch (InterruptedException e1) {
                        e1.printStackTrace();
                    }
                }, "reset-button-style");
                thread.setDaemon(true);
                thread.start();
            }
        });

        content.getChildren().add(itemButton);
    }

}
