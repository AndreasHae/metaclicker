package com.metaclicker.controls.player;

import com.metaclicker.SceneSwitcher;
import com.metaclicker.controls.Utils;
import com.metaclicker.core.Game;
import com.metaclicker.core.Playthrough;
import javafx.fxml.FXML;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.BorderPane;

public class PlayerView extends BorderPane {

    @FXML
    private PointsView pointsView;
    @FXML
    private ItemShop itemShop;
    @FXML
    private WorkerShop workerShop;

    @FXML
    private MenuItem miSave;
    @FXML
    private MenuItem miExit;

    private Playthrough playthrough;

    public PlayerView(Game game) {
        this(new Playthrough(game));
    }

    public PlayerView(Playthrough playthrough) {
        if (playthrough == null) {
            throw new NullPointerException("playthrough must not be null");
        }
        this.playthrough = playthrough;

        Utils.initFxmlElement(this, "/fxml/player/player.fxml");

        miExit.setOnAction(event -> SceneSwitcher.get().goToMain());

        pointsView.pointsProperty().bind(playthrough.pointsProperty());
        pointsView.pointsPerSecondProperty().bind(playthrough.getWorkerManager().pointsPerSecondProperty());
        pointsView.setOnClick(event -> playthrough.incrementPointsByClick());

        itemShop.setItemManager(playthrough.getItemManager());
        workerShop.setWorkerManager(playthrough.getWorkerManager());
    }

}
