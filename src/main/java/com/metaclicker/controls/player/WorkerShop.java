package com.metaclicker.controls.player;

import com.metaclicker.controls.Utils;
import com.metaclicker.core.NotEnoughPointsException;
import com.metaclicker.core.Worker;
import com.metaclicker.core.WorkerManager;
import javafx.beans.binding.Bindings;
import javafx.beans.value.ObservableLongValue;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;

public class WorkerShop extends ScrollPane {

    @FXML
    private VBox content;

    private WorkerManager workerManager;

    public WorkerShop() {
        this(null);
    }

    public WorkerShop(WorkerManager workerManager) {
        Utils.initFxmlElement(this, "/fxml/player/worker-shop.fxml");
        setWorkerManager(workerManager);
    }

    public WorkerManager getWorkerManager() {
        return workerManager;
    }

    public void setWorkerManager(WorkerManager workerManager) {
        this.workerManager = workerManager;
        if (workerManager != null) {
            workerManager.getWorkersInAmounts().forEach(this::addShopItem);
        }
    }

    private void addShopItem(Worker worker, ObservableLongValue amount) {
        content.getChildren().add(new ShopItem(worker, amount));
    }

    private class ShopItem extends AnchorPane {

        private static final double MARGIN_TOP_BOTTOM = 10;
        private static final double LABEL_MARGIN_LEFT = 15;
        private static final double BUTTON_MARGIN_LEFT = 10;

        public ShopItem(Worker worker, ObservableLongValue amount) {
            super();

            setWidth(WorkerShop.this.getMaxWidth());
            setMinHeight(120);

            Label lblName = new Label();
            AnchorPane.setTopAnchor(lblName, MARGIN_TOP_BOTTOM);
            AnchorPane.setLeftAnchor(lblName, LABEL_MARGIN_LEFT);
            lblName.setStyle("-fx-font-size: 14pt");
            lblName.textProperty().bind(worker.nameProperty());
            getChildren().add(lblName);

            Label lblPps = new Label();
            AnchorPane.setTopAnchor(lblPps, MARGIN_TOP_BOTTOM + 30);
            AnchorPane.setLeftAnchor(lblPps, LABEL_MARGIN_LEFT);
            lblPps.textProperty().bind(Bindings.concat("PPS: ", worker.ppsProperty()));
            getChildren().add(lblPps);

            Label lblPrice = new Label();
            AnchorPane.setTopAnchor(lblPrice, MARGIN_TOP_BOTTOM + 50);
            AnchorPane.setLeftAnchor(lblPrice, LABEL_MARGIN_LEFT);
            lblPrice.textProperty().bind(Bindings.concat("Price: ", worker.priceProperty()));
            getChildren().add(lblPrice);

            Label lblAmount = new Label();
            AnchorPane.setTopAnchor(lblAmount, MARGIN_TOP_BOTTOM + 30);
            AnchorPane.setLeftAnchor(lblAmount, LABEL_MARGIN_LEFT + 100);
            lblAmount.textProperty().bind(Bindings.concat("Amount: ", amount));
            getChildren().add(lblAmount);

            Button btnBuy = new Button("Buy");
            AnchorPane.setBottomAnchor(btnBuy, MARGIN_TOP_BOTTOM);
            AnchorPane.setLeftAnchor(btnBuy, BUTTON_MARGIN_LEFT);
            btnBuy.setOnAction(event -> {
                try {
                    workerManager.buy(worker);
                } catch (NotEnoughPointsException e) {
                    btnBuy.setStyle("-fx-border-color: red");
                    Thread thread = new Thread(() -> {
                        try {
                            Thread.sleep(1000);
                            btnBuy.setStyle(null);
                        } catch (InterruptedException e1) {
                            e1.printStackTrace();
                        }
                    });
                    thread.setDaemon(true);
                    thread.start();
                }
            });
            getChildren().add(btnBuy);
        }

    }
}
