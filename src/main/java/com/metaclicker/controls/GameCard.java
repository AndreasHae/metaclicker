package com.metaclicker.controls;

import com.metaclicker.SceneSwitcher;
import com.metaclicker.core.Game;
import com.metaclicker.core.Item;
import com.metaclicker.core.Playthrough;
import com.metaclicker.core.Worker;
import javafx.beans.property.StringProperty;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;

public class GameCard extends VBox {

    @FXML
    private Label lblName;
    @FXML
    private Button btnPlay;

    public GameCard() {
        this("unnamed");
    }

    public GameCard(String name) {
        Utils.initFxmlElement(this, "/fxml/game-card.fxml");

        lblName.setText(name);

        btnPlay.setOnAction(event -> {
            // TODO 2017-06-02: load actual game from file
            Game game = new Game("Ideekay");

            Item iUpgrade1 = new Item("Upgrade 1", 1, 500);
            Item iUpgrade2 = new Item("Upgrade 2", 2, 1000);
            Item iUpgrade3 = new Item("Upgrade 3", 100000, 1000000000);

            Worker wMiner = new Worker("Miner", 5, 1000);
            Worker wBuilder = new Worker("Builder", 2, 500);
            Worker wFarmer = new Worker("Farmer", 1, 100);

            game.getItems().add(iUpgrade1);
            game.getItems().add(iUpgrade2);
            game.getItems().add(iUpgrade3);
            game.getWorkers().add(wMiner);
            game.getWorkers().add(wBuilder);
            game.getWorkers().add(wFarmer);

            Playthrough pt = new Playthrough(game);
            pt.getItemManager().makeAvailable(iUpgrade1);
            pt.getItemManager().makeAvailable(iUpgrade2);
            pt.getItemManager().makeAvailable(iUpgrade3);
            pt.getWorkerManager().makeAvailable(wFarmer);
            pt.getWorkerManager().makeAvailable(wBuilder);
            pt.getWorkerManager().makeAvailable(wMiner);

            SceneSwitcher.get().goToPlayer(pt);
        });
    }

    public String getName() {
        return lblName.getText();
    }

    public void setName(String name) {
        lblName.setText(name);
    }

    public StringProperty nameProperty() {
        return lblName.textProperty();
    }
}
