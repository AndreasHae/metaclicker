package com.metaclicker.controls;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.FlowPane;

import java.io.IOException;

public class OverviewPanel<T extends Node> extends ScrollPane {

    @FXML
    private FlowPane container;

    public OverviewPanel() {
        Utils.initFxmlElement(this, "/fxml/overview-panel.fxml");

        setHbarPolicy(ScrollBarPolicy.NEVER);
        setFitToWidth(true);

        assert container != null;
    }

    public void add(T node) {
        container.getChildren().add(node);
    }

}
