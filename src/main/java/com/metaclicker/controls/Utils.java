package com.metaclicker.controls;

import javafx.fxml.FXMLLoader;
import javafx.scene.Node;

import java.io.IOException;

public class Utils {

    public static  <T extends Node> void initFxmlElement(T node, String pathToFxml) {
        FXMLLoader loader = new FXMLLoader(node.getClass().getResource(pathToFxml));
        loader.setRoot(node);
        loader.setController(node);

        try {
            loader.load();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
