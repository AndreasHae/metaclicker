package com.metaclicker.controllers;

import com.metaclicker.controls.GameCard;
import com.metaclicker.controls.GameCreationForm;
import com.metaclicker.controls.OverviewPanel;
import com.metaclicker.controls.PlaythroughCard;
import javafx.fxml.FXML;

public class MainController {

    @FXML
    private OverviewPanel<GameCard> gamePanel;
    @FXML
    private OverviewPanel<PlaythroughCard> playthroughPanel;
    @FXML
    private GameCreationForm gameCreationForm;

    public void initialize() {
        // TODO: Add cards for available games to panel
        gamePanel.add(new GameCard());
        // TODO: Add cards for available playthroughs to panel
        playthroughPanel.add(new PlaythroughCard());
    }

}
