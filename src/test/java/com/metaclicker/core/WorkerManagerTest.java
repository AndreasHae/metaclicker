package com.metaclicker.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class WorkerManagerTest {

    private static final long WORKER_PRICE = 10;
    private Game game;
    private Playthrough playthrough;
    private WorkerManager workerManager;
    private Worker worker;

    @BeforeEach
    void setUp() {
        game = new Game("Game");
        playthrough = new Playthrough(game);
        workerManager = new WorkerManager(playthrough);
        worker = new Worker("John", 1, WORKER_PRICE);
    }

    @Test
    void testBuyWorker() {
        game.getWorkers().add(worker);
        workerManager.makeAvailable(worker);
        playthrough.setPoints(WORKER_PRICE);
        try {
            workerManager.buy(worker);
        } catch (NotEnoughPointsException e) {
            fail("falsely throws NotEnoughPointsException");
        }

        assertEquals(1, workerManager.getNumberOfWorkers(worker));
    }

    @Test
    void testBuyWorkerNotEnoughCookies() {
        game.getWorkers().add(worker);
        workerManager.makeAvailable(worker);
        assertThrows(NotEnoughPointsException.class,
                () -> workerManager.buy(worker));
    }

    @Test
    void testBuyWorkerNotAvailable() {
        game.getWorkers().add(worker);
        playthrough.setPoints(WORKER_PRICE);
        assertThrows(IllegalStateException.class,
                () -> workerManager.buy(worker));
    }

    @Test
    void testBuyWorkerNotInGame() {
        assertThrows(NotInGameException.class,
                () -> workerManager.buy(worker));
    }

    @Test
    void testGetNumberOfWorkersWorkerUnavailable() {
        game.getWorkers().add(worker);
        assertEquals(-1, workerManager.getNumberOfWorkers(worker));
    }
}