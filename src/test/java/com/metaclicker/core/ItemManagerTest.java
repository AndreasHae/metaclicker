package com.metaclicker.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ItemManagerTest {

    private static final long ITEM_PRICE = 10;
    private Game game;
    private Playthrough playthrough;
    private ItemManager itemManager;
    private Item item;

    @BeforeEach
    void setUp() {
        game = new Game("Game");
        playthrough = new Playthrough(game);
        itemManager = new ItemManager(playthrough);
        item = new Item("Finger", 1, ITEM_PRICE);
    }

    @Test
    void testBuyItem() {
        game.getItems().add(item);
        itemManager.makeAvailable(item);
        playthrough.setPoints(ITEM_PRICE);
        try {
            itemManager.buyItem(item);
        } catch (NotEnoughPointsException e) {
            fail("falsely throws NotEnoughPointsException");
        }

        assertEquals(true, itemManager.isItemBought(item));
    }

    @Test
    void testBuyItemNotEnoughCookies() {
        game.getItems().add(item);
        itemManager.makeAvailable(item);
        assertThrows(NotEnoughPointsException.class,
                () -> itemManager.buyItem(item));
    }

    @Test
    void testBuyItemNotAvailable() {
        game.getItems().add(item);
        assertThrows(IllegalStateException.class,
                () -> itemManager.buyItem(item));
    }

    @Test
    void testBuyItemNotInGame() {
        assertThrows(NotInGameException.class,
                () -> itemManager.buyItem(item));
    }

    @Test
    void testIsItemAvailable() {
        game.getItems().add(item);
        assertEquals(false, itemManager.isItemAvailable(item));
        itemManager.makeAvailable(item);
        assertEquals(true, itemManager.isItemAvailable(item));
    }

    @Test
    void testIsItemBoughtItemUnavailable() {
        game.getItems().add(item);
        assertEquals(false, itemManager.isItemBought(item));
    }

}