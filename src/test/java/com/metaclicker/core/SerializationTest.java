package com.metaclicker.core;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.io.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

@Disabled
class SerializationTest {

    @Test
    void testSerializeItem() throws IOException, ClassNotFoundException {
        Item item = new Item("Test item", 10, 100);

        PipedOutputStream os = new PipedOutputStream();
        PipedInputStream is = new PipedInputStream(os);

        ObjectOutputStream outputStream = new ObjectOutputStream(os);
        ObjectInputStream inputStream = new ObjectInputStream(is);

        outputStream.writeObject(item);
        Item deserialized = (Item) inputStream.readObject();

        assertEquals(item, deserialized);

        os.close();
        is.close();
    }

    @Test
    void testSerializeWorker() throws IOException, ClassNotFoundException {
        Worker worker = new Worker("Test worker", 10, 100);

        PipedOutputStream os = new PipedOutputStream();
        PipedInputStream is = new PipedInputStream(os);

        ObjectOutputStream outputStream = new ObjectOutputStream(os);
        ObjectInputStream inputStream = new ObjectInputStream(is);

        outputStream.writeObject(worker);
        Worker deserialized = (Worker) inputStream.readObject();

        assertEquals(worker, deserialized);

        os.close();
        is.close();
    }

    @Test
    void testSerializeGame() throws IOException, ClassNotFoundException {
        Game game = new Game("TestGame");
        game.getItems().add(new Item("i1", 10, 20));
        game.getWorkers().add(new Worker("w1", 2, 10));

        PipedOutputStream os = new PipedOutputStream();
        PipedInputStream is = new PipedInputStream(os);

        ObjectOutputStream outputStream = new ObjectOutputStream(os);
        ObjectInputStream inputStream = new ObjectInputStream(is);

        outputStream.writeObject(game);
        Game deserialized = (Game) inputStream.readObject();

        // equals() only compares the games' names, but toString also contains workers and items, so we compare those
        assertEquals(game.toString(), deserialized.toString());

        outputStream.close();
        inputStream.close();
    }

    @Test
    void testSerializePlaythrough() throws IOException, ClassNotFoundException {
        Game game = new Game("TestGame");
        game.getItems().add(new Item("i1", 10, 20));
        game.getWorkers().add(new Worker("w1", 2, 10));

        Playthrough playthrough = new Playthrough(game);

        PipedOutputStream os = new PipedOutputStream();
        PipedInputStream is = new PipedInputStream(os);

        ObjectOutputStream outputStream = new ObjectOutputStream(os);
        ObjectInputStream inputStream = new ObjectInputStream(is);

        outputStream.writeObject(playthrough);
        Playthrough deserialized = (Playthrough) inputStream.readObject();

        assertEquals(playthrough, deserialized);

        outputStream.close();
        inputStream.close();
    }

}