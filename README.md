# Metaclicker

## What is it?
A program that can be used to create and play incremental games like
[Cookie Clicker](http://orteil.dashnet.org/cookieclicker/) or
[Bitcoin Billionaire](https://play.google.com/store/apps/details?id=com.noodlecake.bitcoin&hl=de).

## Features
- Creating custom made clicker games with a fancy GUI
- Saving and playing those games
- Inserting custom images
- Saving while playing the game

### If enough time is left
- Publishing created games onto a cloud
- Playing games other people created
- Rating and writing reviews of games other people created

### If there is still enough time left
- Web interface
- Android app